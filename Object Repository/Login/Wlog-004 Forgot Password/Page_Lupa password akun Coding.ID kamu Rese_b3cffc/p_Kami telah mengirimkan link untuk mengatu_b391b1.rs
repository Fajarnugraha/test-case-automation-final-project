<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Kami telah mengirimkan link untuk mengatu_b391b1</name>
   <tag></tag>
   <elementGuidId>6bcc012d-c18a-4fd0-880c-7b1517e3662d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>p.text-muted</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>b12e397c-f803-45bc-9e59-b2a045f0a0ad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-muted</value>
      <webElementGuid>920172cb-261f-43bb-950b-2172691e01c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        Kami telah mengirimkan link untuk mengatur ulang kata sandi anda.Silahkan periksa email anda
                    </value>
      <webElementGuid>cf97c385-e679-48e1-a371-4269d310c522</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;no-htmlimports no-flash no-proximity no-applicationcache blobconstructor blob-constructor cookies cors no-customprotocolhandler dataview eventlistener geolocation history no-ie8compat json notification queryselector no-serviceworker customevent postmessage svg templatestrings typedarrays websockets no-xdomainrequest webaudio webworkers no-contextmenu cssall audio canvas canvastext contenteditable emoji olreversed no-userdata video no-vml webanimations webgl adownload audioloop canvasblending todataurljpeg todataurlpng todataurlwebp canvaswinding no-ambientlight hashchange inputsearchevent pointerevents no-hiddenscroll mathml unicoderange no-touchevents no-unicode no-batteryapi no-battery-api no-crypto no-dart gamepads fullscreen indexeddb indexeddb-deletedatabase intl pagevisibility performance pointerlock quotamanagement requestanimationframe raf vibrate no-webintents no-lowbattery getrandomvalues backgroundblendmode cssanimations backdropfilter backgroundcliptext appearance exiforientation audiopreload&quot;]/body[1]/div[@class=&quot;wm-main-wrapper&quot;]/div[@class=&quot;wm-main-section&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;___class_+?3___&quot;]/p[@class=&quot;text-muted&quot;]</value>
      <webElementGuid>42934827-ce83-4de2-8cf8-8b2d96e8e7b1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lupa Kata Sandi'])[1]/following::p[1]</value>
      <webElementGuid>46749a2e-f82a-4856-9351-b16119281cfd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kirim link'])[1]/preceding::p[1]</value>
      <webElementGuid>c6799771-f389-4267-97a0-e77ad9393747</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='About CODING.ID'])[1]/preceding::p[2]</value>
      <webElementGuid>a6ce3a15-277e-4a9e-9996-09fb47410654</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Kami telah mengirimkan link untuk mengatur ulang kata sandi anda.']/parent::*</value>
      <webElementGuid>6296ae2a-2832-4397-9c3f-48c35e1e8a2d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div[2]/p</value>
      <webElementGuid>b7d8f2ff-4839-42f2-9bcd-4df04595e567</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = '
                        Kami telah mengirimkan link untuk mengatur ulang kata sandi anda.Silahkan periksa email anda
                    ' or . = '
                        Kami telah mengirimkan link untuk mengatur ulang kata sandi anda.Silahkan periksa email anda
                    ')]</value>
      <webElementGuid>a92d62cf-bd85-410a-aaad-3e6259c1ab99</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
