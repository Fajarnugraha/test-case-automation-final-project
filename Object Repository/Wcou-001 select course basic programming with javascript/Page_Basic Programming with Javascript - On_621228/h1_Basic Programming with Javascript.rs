<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Basic Programming with Javascript</name>
   <tag></tag>
   <elementGuidId>4e21e357-9229-4257-a880-79f68a0c7cd8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//h1[@id='jumbotronTitle']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#jumbotronTitle</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>59bf267f-d0a0-427f-9d18-c68a42f4b368</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>jumbotronTitle</value>
      <webElementGuid>3021d19b-998c-4195-8f1e-7d9758f99bbb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Basic Programming with Javascript
        </value>
      <webElementGuid>4e186fd8-b1ec-45dd-bf2b-d32752417e5e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;jumbotronTitle&quot;)</value>
      <webElementGuid>89f81593-1cd2-4a97-b97d-100ae4fac1d2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//h1[@id='jumbotronTitle']</value>
      <webElementGuid>010ee7d5-85e1-411f-a71f-1914637e2ac8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='jumbotronCourseDetail']/div/h1</value>
      <webElementGuid>ccd1e9b4-2d3a-426a-a3d0-06e4413bc699</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Back to Course >'])[1]/preceding::h1[1]</value>
      <webElementGuid>0d553119-2129-4deb-aff4-366ad200e732</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Basic Programming with Javascript']/parent::*</value>
      <webElementGuid>50a2aca6-37d8-4a36-9257-adbb9933eac4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>fcd75388-4701-4f63-942e-c527e8c3021a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[@id = 'jumbotronTitle' and (text() = 'Basic Programming with Javascript
        ' or . = 'Basic Programming with Javascript
        ')]</value>
      <webElementGuid>e28971c2-dcb1-494c-a9d2-3fda3a9fce9e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
