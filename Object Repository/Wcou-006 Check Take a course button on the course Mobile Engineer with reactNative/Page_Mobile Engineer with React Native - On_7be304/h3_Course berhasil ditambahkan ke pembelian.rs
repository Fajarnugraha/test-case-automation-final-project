<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h3_Course berhasil ditambahkan ke pembelian</name>
   <tag></tag>
   <elementGuidId>546cdc2b-89d9-4209-a9ce-8268e71af3ca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//h3[@id='info']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#info</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>250bcb74-13fd-492f-aacd-d40638c1ea18</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>info</value>
      <webElementGuid>666e21cf-010f-4fa9-8fb4-eee306c651e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Course berhasil ditambahkan ke pembelian!</value>
      <webElementGuid>b13f9661-17b1-45b1-8b46-c726d3823a0a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;info&quot;)</value>
      <webElementGuid>1a2c39d8-f700-4a34-ac7e-d95420a5ad35</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//h3[@id='info']</value>
      <webElementGuid>0646512d-bad0-4ee9-8b38-acfd978f5dcb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id=' Modal_Success']/div/div[2]/h3</value>
      <webElementGuid>9e5eca88-8a2e-459b-bf47-788b7a8b95bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tutup'])[2]/following::h3[1]</value>
      <webElementGuid>bd812510-291d-4752-836c-ae29d42b66c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mobile Engineer with React Native'])[3]/preceding::h3[1]</value>
      <webElementGuid>b41d3ced-6465-4b80-a3a2-b40a867e6c70</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Course berhasil ditambahkan ke pembelian!']/parent::*</value>
      <webElementGuid>f7942389-5b5f-4488-84e2-cab657bdf8d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div[2]/h3</value>
      <webElementGuid>6ee9109a-993a-41b9-87b9-7ad1b7c7c2b3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[@id = 'info' and (text() = ' Course berhasil ditambahkan ke pembelian!' or . = ' Course berhasil ditambahkan ke pembelian!')]</value>
      <webElementGuid>a66737b3-7ddb-4d35-b9fc-8c1233daf945</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
