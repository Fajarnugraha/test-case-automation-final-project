<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Konfirmasi kata sandi_password_confirmation</name>
   <tag></tag>
   <elementGuidId>5e678e41-3267-42b5-a053-fd07d047ef57</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='password-confirm']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#password-confirm</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>f27493e4-25c3-4bd7-9ea3-048f2166d3db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>password-confirm</value>
      <webElementGuid>40c17a5c-79db-48bf-97d5-2c4e24fbb3dd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>a01dd7bf-826d-437f-9a2b-8c9e45f99ddb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Ulangi kata sandi anda</value>
      <webElementGuid>4a1c0291-7bb8-4541-b543-c58121adb43d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>db93654a-aa9f-4a88-85f0-1babaf260326</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>password_confirmation</value>
      <webElementGuid>8818c415-c232-48e1-87b1-b3a1ab6b9706</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>new-password</value>
      <webElementGuid>b13f2aa1-6855-4f4e-950c-2450bf8cc4e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;password-confirm&quot;)</value>
      <webElementGuid>1336f0fe-fa60-42b5-bb19-5d7bfcdc29ce</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='password-confirm']</value>
      <webElementGuid>62616ac6-cfb5-4d88-ad53-7850186ab019</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/input</value>
      <webElementGuid>b441b47e-39fe-4768-9802-5bf426b24edf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'password-confirm' and @type = 'password' and @placeholder = 'Ulangi kata sandi anda' and @name = 'password_confirmation']</value>
      <webElementGuid>04b46b24-417f-417d-8499-a5d96bf8d970</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
